package com.wh2.notifier.dist.rest;

import com.wh2.notifier.services.EmailService;
import com.wh2.notifier.services.contract.to.EmailTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Operations over the email notifier")
@RestController
@RequestMapping("/email")
public class EmailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailService;

    /**
    Json Example:
    {
    "email":["email@gmail.com"],
    "template":"aclWelcome",
    "fields":
        {
            "firstName":"Pepe",
            "name":"Gomez",
            "token":"1234",
            "code": "1234"
        }
     **/

    @ApiOperation(value = "send email notification")
    @PostMapping("/send")
    public ResponseEntity send(
                                @ApiParam(value = "The email parameters", required = true)
                                @RequestBody EmailTO to) {
        LOGGER.info("Hello from EmailController : send");
        try{
            return new ResponseEntity(this.emailService.sendEmail(to), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage() , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
