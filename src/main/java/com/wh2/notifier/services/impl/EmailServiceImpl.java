package com.wh2.notifier.services.impl;

import com.wh2.notifier.services.EmailService;
import com.wh2.notifier.services.contract.to.EmailTO;
import com.wh2.notifier.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;

@Service("EmailService")
public class EmailServiceImpl implements EmailService {
    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private SpringTemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String from;
    @Value("${spring.mail.host}")
    private String host;
    @Value("${spring.mail.port}")
    private String port;
    @Value("${spring.mail.password}")
    private String password;

    @Override
    public String sendEmail(EmailTO emailTO) throws MessagingException {

        try {
            var response = "";
            if(emailTO != null){
                logger.info("emailTo: {}", emailTO);
                logger.debug("enviando correo a: {}", emailTO.getEmail()[0]);
                final var ctx = new Context();
                ctx.setVariables(emailTO.getFields());
                final String htmlContent = this.templateEngine.process( emailTO.getTemplate(), ctx);

                var message = emailSender.createMimeMessage();
                var helper = new MimeMessageHelper(message, true);
                helper.setFrom(from);
                helper.setTo(emailTO.getEmail());
                helper.setText(htmlContent,true);
                response = "Email Enviado";

                switch (emailTO.getTemplate()) {
                    case Constant.RECOVER_PASSWORD:
                        helper.setSubject("Recupere su contraseña");
                        response = "Email de recuperación de clave enviado correctamente";
                        break;
                    case Constant.BLOCKED_ACCOUNT:
                        helper.setSubject("Contraseña Temporal");
                        response = "Email con clave termporal enviado correctamente";
                        break;
                    case Constant.CHECKLIST:
                        helper.setSubject(emailTO.getFields().get("subject").toString());
                        response = "Email con checklist enviado correctamente";
                        break;
                    default:
                        break;
                }

                emailSender.send(message);
                logger.debug("correo enviado satisfactoriamente a: {}",emailTO.getEmail()[0]);
                logger.info("Exit from EmailServiceImpl : sendEmail");
            }else{
                logger.warn("emailTo: null");
            }
            return response;
        }catch (Exception e){
            logger.error("no se pudo enviar el correo, debido a: {}",e.getMessage());
            throw new MessagingException();
        }
    }
}
