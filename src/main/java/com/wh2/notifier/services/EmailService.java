package com.wh2.notifier.services;

import com.wh2.notifier.services.contract.to.EmailTO;

import javax.mail.MessagingException;

public interface EmailService {
    String sendEmail(EmailTO emailTO) throws MessagingException;
}
