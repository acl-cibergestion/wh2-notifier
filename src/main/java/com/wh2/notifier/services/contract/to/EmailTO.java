package com.wh2.notifier.services.contract.to;

import java.util.Map;

public class EmailTO {
    private String[] email;
    private String template;
    private Map<String, Object> fields;

    public String[] getEmail() {
        return email;
    }

    public void setEmail(String[] email) {
        this.email = email;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }

    public EmailTO withTemplate(String template){
        this.setTemplate(template);
        return(this);
    }
}
