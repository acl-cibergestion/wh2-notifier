package com.wh2.notifier.util;

public class Constant {

    private Constant(){

    }

    public static final String RECOVER_PASSWORD="recoverPassword";
    public static final String BLOCKED_ACCOUNT="blockedAccount";
    public static final String CHECKLIST="checklist";

}
